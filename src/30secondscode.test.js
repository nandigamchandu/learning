const secCode = require('./30secondscodes')

describe('Testing all function', () => {
  test('function returns true if collection has all truly element', () => {
    expect(secCode.all([true, 1, 'Lambda', {}])).toBe(true)
  })

  test('function returns false if collection has at-least one falsy element', () => {
    expect(secCode.all([0, true, true, true])).toBe(false)
  })

  test('function returns true if predicate return true for all elements in collection', () => {
    expect(secCode.all([2, 4, 6, 5], x => x > 1)).toBe(true)
  })

  test('function returns false if predicate return false for at-least one element in collection', () => {
    expect(secCode.all([1, 4, 6, 5], x => x > 1)).toBe(false)
  })
})

describe('Testing countBy', () => {
  test('function returns {4: 1, 6:2} by grouping elements in the collection base on the given function  ', () => {
    expect(secCode.countBy([6.1, 4.2, 6.3], Math.floor)).toEqual({ 4: 1, 6: 2 })
  })
  test('function return empty object if collection is empty ', () => {
    expect(secCode.countBy([], Math.floor)).toEqual({})
  })
  test('Error throw by the function when parameter are miss match', () => {
    expect(secCode.countBy).toThrow(TypeError)
  })

  test('function group some elements as NaN, when given function returns NaN for that elements', () => {
    expect(
      secCode.countBy([1, 2, 3, 4, 5, 'even', 'odd'], val => val % 2),
    ).toEqual({ 0: 2, 1: 3, NaN: 2 })
  })

  test('function group some elements as undefine, when given function returns undefine for that elements', () => {
    expect(
      secCode.countBy([1, 2, 3, 4, 5, 'even', 'odd'], val => val.length),
    ).toEqual({ 3: 1, 4: 1, undefined: 5 })
  })
})

describe('Testing indexOfAll', () => {
  test('function returns indexes of the given element', () => {
    expect(secCode.indexOfAll([1, 2, 3, 1, 2, 3], 1)).toEqual([0, 3])
  })
  test('function returns empty collection if given element is not present in collection', () => {
    expect(secCode.indexOfAll([1, 2, 3, 1, 2, 3], 5)).toEqual([])
  })
  test('Exception throw by the function', () => {
    expect(secCode.indexOfAll).toThrow(TypeError)
  })
})

describe('Testing join', () => {
  test('function returns string with the given separator and end', () => {
    expect(
      secCode.join(['pen', 'pineapple', 'apple', 'cat'], ',', '&'),
    ).toMatch('pen,pineapple,apple&cat')
  })
  test('function returns string with comma separator if separator and end are not provided', () => {
    expect(secCode.join(['pen', 'pineapple', 'apple', 'cat'])).toMatch(
      'pen,pineapple,apple,cat',
    )
  })
  test('testing function other than comma separator', () => {
    expect(secCode.join(['pen', 'pineapple', 'apple', 'cat'], ':')).toMatch(
      /pen:pineapple/,
    )
  })
  test('function return undefine if we supply empty collection', () => {
    expect(secCode.join([], ':')).toBeDefined()
  })
  test('function returns single word if collection has on element', () => {
    expect(secCode.join(['javascript'], ':')).toMatch('javascript')
  })
  test('Error throw by the function', () => {
    expect(secCode.join).toThrow(TypeError)
  })
})

describe('Testing offset', () => {
  test('function shifts element towards left for the given number of elements ', () => {
    expect(secCode.offset([1, 2, 3, 4, 5], 2)).toEqual([3, 4, 5, 1, 2])
  })
  test('function shifts element towards right for the given number of elements', () => {
    expect(secCode.offset([1, 2, 3, 4, 5], -2)).toEqual([4, 5, 1, 2, 3])
  })
  test('function returns same input collection irrespective of moves if collection has only one element', () => {
    expect(secCode.offset([1], 2)).toEqual([1])
  })
  test('Testing function on string', () => {
    expect(secCode.offset('Chandu', 2)).toEqual(['a', 'n', 'd', 'u', 'C', 'h'])
  })
  test('Error throw by the function', () => {
    expect(secCode.offset).toThrow(TypeError)
  })
})

describe('Testing dropRightWhile', () => {
  test('function drop elements from the right until given function returns true for the given element', () => {
    expect(secCode.dropRightWhile([1, 2, 3, 4, 5, 6], x1 => x1 < 4)).toEqual([
      1,
      2,
      3,
    ])
  })
  test('function does not drop any element if given function return true for last element', () => {
    expect(secCode.dropRightWhile([1, 2, 4, 5, 1], x1 => x1 < 4)).toEqual([
      1,
      2,
      4,
      5,
      1,
    ])
  })
  test('function returns empty element given collection is empty', () => {
    expect(secCode.dropRightWhile([], x1 => x1 < 4)).toEqual([])
  })
  test('function return empty string if we supply string instead of array', () => {
    expect(secCode.dropRightWhile('javascript', x1 => x1 < 4)).toEqual('')
  })
})

describe('Testing everyNth', () => {
  test('function return every 2nd element from given collection', () => {
    expect(secCode.everyNth([1, 2, 3, 4, 5, 6, 7], 2)).toEqual([2, 4, 6])
  })
  test('function returns empty collection if length of array is less than given nth element', () => {
    expect(secCode.everyNth([1, 2, 3, 4, 5, 6, 7], 10)).toEqual([])
  })
  test('function returns  empty collection if nth element is 0', () => {
    expect(secCode.everyNth([1, 2, 3], 0)).toEqual([])
  })
  test('function returns empty collection if nth element is -ve', () => {
    expect(secCode.everyNth([1, 2, 3], -1)).toEqual([])
  })
})

describe('Testing initializeArrayWithRange', () => {
  test('function returns collection with elements start from 0 to 5', () => {
    expect(secCode.initializeArrayWithRange(5)).toEqual([0, 1, 2, 3, 4, 5])
  })
  test('function returns collection with elements start from 3 to 5', () => {
    expect(secCode.initializeArrayWithRange(5, 3)).toEqual([3, 4, 5])
  })
  test('function returns collection with elements start from 0 to 5 with step 2', () => {
    expect(secCode.initializeArrayWithRange(5, 0, 2)).toEqual([0, 2, 4])
  })
  test('function returns empty array if starting element greater than ending element ', () => {
    expect(secCode.initializeArrayWithRange(5, 10, 2)).toEqual([])
  })

  test('function return empty array if we do not proved any arguments', () => {
    expect(secCode.initializeArrayWithRange()).toEqual([])
  })
  test('function return empty array if step is greater than end - start', () => {
    expect(secCode.initializeArrayWithRange(5, 0, 7)).toEqual([])
  })
})

describe('Testing findLast', () => {
  test('function returns last element which returns true for given function', () => {
    expect(secCode.findLast([1, 2, 3, 4, 5], val => val % 2 == 0)).toBe(4)
  })
  test('function returns undefine if no element satisfy function', () => {
    expect(
      secCode.findLast([1, 3, 5, 7], val => val % 2 == 0),
    ).not.toBeDefined()
  })
  test('function returns undefine for empty array', () => {
    expect(secCode.findLast([], val => val % 2 == 0)).not.toBeDefined()
  })
  test('Error throw by the function', () => {
    expect(secCode.findLast).toThrow(TypeError)
  })
})

describe('Testing initialize2DArray', () => {
  test('Function returns 2 x 2 Array with zeros', () => {
    expect(secCode.initialize2DArray(2, 2, 0)).toEqual([[0, 0], [0, 0]])
  })
  test('Function return 1 x 3 Array with zeros', () => {
    expect(secCode.initialize2DArray(1, 3, 0)).toEqual([[0, 0, 0]])
  })
  test('Function return 3 x 1 Array with zeros', () => {
    expect(secCode.initialize2DArray(3, 1, 'a')).toEqual([['a'], ['a'], ['a']])
  })
  test('Function return 2 x 2 Array with nulls', () => {
    expect(secCode.initialize2DArray(2, 2)).toEqual([
      [null, null],
      [null, null],
    ])
  })
})
