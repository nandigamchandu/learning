import t from 'tcomb'
import { toObject } from 'tcomb-doc'
import _ from 'lodash'

const Person = t.struct(
  {
    name: t.String,
    age: t.maybe(t.Number),
  },
  'User',
)

console.log(JSON.stringify(toObject(Person), null, 2))

// Get started: basic type checking

function sum(a, b) {
  return a + b
}

console.log(sum(5, 6))

// An easy way to achieve this goal is to add asserts

function sumWithAssert(a, b) {
  t.assert(typeof a === 'number', 'argument a is not a number')
  t.assert(typeof b === 'number', 'argument b is not a number')
  return a + b
}

console.log(sumWithAssert(11, 10))

// Reducing the boilerplate

function sumWithAssertTcomb(a, b) {
  t.assert(t.Number.is(a), 'argument a is not a number')
  t.assert(t.Number.is(b), 'argument b is not a number')
  return a + b
}

console.log(sumWithAssertTcomb(11, 10))

function sunWithAssertTcomb2(a, b) {
  t.Number(a)
  t.Number(b)
  return a + b
}

console.log(sunWithAssertTcomb2(10, 56))

// the func combinator

const SumType = t.func([t.Number, t.Number], t.Number)

const sumWithAssertTcombFunc = SumType.of((a, b) => a + b)

console.log(sumWithAssertTcombFunc(1, 2))

// User sefine types, the irreducible combinator

const MapType = t.irreducible('MapType', x => x instanceof Map)

function size(map) {
  MapType(map)
  return map.size
}

console.log(size(new Map()))

// Restricting a type, the refinement combinator

const PasswordIrreducible = t.irreducible(
  'Password',
  x => t.String.is(x) && x.length > 6,
)

const PasswordRefinement = t.refinement(t.String, s => s.length > 6)

PasswordIrreducible('password')
PasswordRefinement('Password')

// Chaining refinements

const Integer = t.refinement(t.Number, n => n % 1 === 0, 'Integer')
const PositiveInteger = t.refinement(Integer, i => i > 0, 'PositiveInteger')
const Rating = t.refinement(PositiveInteger, r => r <= 5, 'Rating')
Rating(1)

// A first taste of runtime type introspection
console.log(Integer.meta)

function getTypeChain(type) {
  const name = type.meta.name
  const supertype = type.meta.type
  if (!supertype) {
    return name
  }
  return [name].concat(getTypeChain(supertype))
}

console.log(getTypeChain(Rating))

// A Particular Kind of refinement, the enums combinator

const Country = t.enums({ IT: 'Italy', US: 'United States' }, 'Country')

Country('IT')
console.log(JSON.stringify(Country.meta.map))

// The of static function
const Country1 = t.enums.of('IT US IN', 'Country')
const Country2 = t.enums.of(['IT', 'US', 'IN'], 'Country')

Country1('IN')
Country2('US')

// Optional values, the maybe combinator
t.maybe(Country1)()
t.maybe(Country1)(undefined)
t.maybe(Country1)(null)
t.maybe(Country1)('IT')

// Classes, the struct combinator
const Point = t.struct({ x: t.Number, y: t.Number }, 'Point')

const point = Point({ x: 1, y: 2 })

// Method are define as usual

Point.prototype.toString = function() {
  return `(${this.x}, ${this.y})`
}

console.log(String(point))

const emailRegExp = /.*@gmail.com$/
const Email = t.refinement(t.String, s => emailRegExp.test(s, 'Email'))

const Role = t.enums.of(['admin', 'guest'], 'Role')
const User = t.struct(
  {
    id: t.String,
    email: Email,
    role: Role,
    birthDate: t.maybe(t.Date),
    name: t.maybe(t.String),
    surename: t.maybe(t.String),
  },
  'User',
)

const user = User({
  id: 'A40',
  email: 'user@gmail.com',
  role: 'admin',
  name: 'Giulio',
})
console.log(JSON.stringify(user))

// structs can be nested

const Anagraphic = t.struct(
  {
    birthDate: t.maybe(t.Date),
    name: t.maybe(t.String),
    surename: t.maybe(t.String),
  },
  'Anagraphic',
)

const User1 = t.struct(
  {
    id: t.String,
    email: Email,
    role: Role,
    anagraphic: Anagraphic,
  },
  'User',
)

const user1 = User1({
  id: 'A40',
  email: 'user@gmail.com',
  role: 'admin',
  anagraphic: {
    name: 'Giulio',
  },
})

console.log(JSON.stringify(user1))

// Refinement can be applied to all types

const BaseAnagraphic = t.struct(
  {
    birthDate: t.maybe(t.Date),
    name: t.maybe(t.String),
    surename: t.maybe(t.String),
  },
  'BaseAnagraphic',
)

const Anagraphic1 = t.refinement(
  BaseAnagraphic,
  x => t.Nil.is(x.name) === t.Nil.is(x.surename),
  'Anagraphic',
)

const User2 = t.struct(
  {
    id: t.String,
    email: Email,
    role: Role,
    anagraphic: Anagraphic1,
  },
  'User',
)

const user2 = User2({
  id: 'A40',
  email: 'user@gmail.com',
  role: 'admin',
  anagraphic: {
    name: 'Giulio',
    surename: 'Canti',
  },
})
console.log(JSON.stringify(user2))

const user3 = User2({
  id: 'A40',
  email: 'user@gmail.com',
  role: 'admin',
  anagraphic: {},
})

// Runtime type introspection, playing with structs

// serialise an instance of user.
console.log(JSON.stringify(user3))

// Deserialising is easy as well since struct constructs
const json = JSON.parse(JSON.stringify(user3))
console.log(User2(json))

const user4 = User2({
  id: 'A40',
  email: 'user@gmail.com',
  role: 'admin',
  anagraphic: {
    birthDate: new Date(1992, 1, 14),
  },
})

const json2 = JSON.parse(JSON.stringify(user4))
// console.log(User2(json2))

function deserialize(value, type) {
  if (t.Function.is(type.fromJSON)) {
    return type.fromJSON(value)
  }
  const { kind } = type.meta
  switch (kind) {
    case 'struct':
      return type(
        _.mapValues(value, (v, k) => deserialize(v, type.meta.props[k])),
      )
    case 'maybe':
      return t.Nil.is(value) ? null : deserialize(value, type.meta.type)
    case 'subtype':
      return deserialize(value, type.meta.type)
    default:
      return value
  }
}

t.Date.fromJSON = s => new Date(s)
console.log(deserialize(json2, User2))
