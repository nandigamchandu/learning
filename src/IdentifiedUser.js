import t from 'tcomb'
import User from './User'

export default User.extend({ id: t.String }, 'IdentifiedUser')
