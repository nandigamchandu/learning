import t from 'tcomb'
import User from './User'

const login = t.struct(
  {
    email: User.meta.props.email,
    message: t.String,
  },
  'Message',
)

const mylogin = login({
  email: 'example@gmail.com',
  message: 'test message',
})

console.log(JSON.stringify(mylogin))
