// All

const all: <T>(arr: T[], fn?: (value: T) => boolean) => boolean = (
  arr,
  fn = Boolean,
) => arr.every(fn)

console.log('all', all([5, 2, 3, 4], x => x > 1))
console.log('all', all([true, true, true, true]))
console.log('all', all([true, true, false, true]))

// allEqual
const allEqual: <T>(arr: T[]) => boolean = arr => arr.every(x => x === arr[0])
console.log('allEqual', allEqual([1, 2, 3, 4, 5, 6]))
console.log('allEqual', allEqual([1, 1, 1, 1, 1, 1]))
console.log('allEqual', allEqual([true, true, true, true, true, true]))

// any
const any: <T>(arr: T[], fn?: (value: T) => boolean) => boolean = (
  arr,
  fn = Boolean,
) => arr.some(fn)
console.log('any', any([5, 2, 3, 4], x => x % 2 == 0))
console.log('any', any([true, true, true, true]))
console.log('any', any([true, true, false, true]))
console.log('any', any([false, false, false, false]))

// arrayToCSV
const arrayToCSV: <T>(arr2d: T[][], delimited?: string) => string = (
  arr2d,
  delimiter = ',',
) => {
  return arr2d
    .map(arr1d =>
      arr1d.map(ele => {
        if (typeof ele === 'string') {
          return `"${ele.replace(/"/g, '""')}"`
        } else {
          return ele
        }
      }),
    )
    .join('\n')
}
console.log(arrayToCSV([['a', 'b'], ['c', 'd']]))
console.log(arrayToCSV([[1, 2], [3, 4]]))
console.log(arrayToCSV([[1, 2], [3, 'chandu']]))
console.log(arrayToCSV([['a', '"b" great'], ['c', 3.1415]]))

// bifurcateBy
const bifurcate: <T>(arr: T[], filter: (value: T) => boolean) => T[][] = (
  arr,
  filter,
) => {
  let result: typeof arr[] = [[], []]
  return arr.reduce(
    (acc, cur) => (acc[filter(cur) ? 0 : 1].push(cur), acc),
    result,
  )
}

console.log(bifurcate([1, 2, 3, 4], x => x % 2 == 0))
console.log(
  bifurcate(['chandu', 'charm', 'change', 'chain'], x => x == 'chandu'),
)

// cumsum
const sum: (arr: number[]) => number = arr =>
  arr.reduce((acc, cur) => acc + cur, 0)
const cumsum: (arr: number[]) => number[] = arr => {
  let result: number[] = []
  return arr.reduce((acc, cur) => (acc.push(sum(acc) + cur), acc), result)
}

console.log(cumsum([1, 2, 3, 4, 5, 6]))

// practice example on reduce
interface Data {
  header: string[]
  value: (string | number)[][]
}
let data: Data = {
  header: ['id', 'first_name', 'last_name'],
  value: [
    [100, 'Durga', 'Turaga'],
    [101, 'Kalyan', 'Kundu'],
    [102, 'Krishna', 'Rao'],
  ],
}

const myObject: (
  header: string[],
  value: (string | number)[],
) => { [key: string]: string | number } = (header, value) => {
  let result: { [key: string]: string | number } = {}
  return header.reduce((acc, cur, i) => ((acc[cur] = value[i]), acc), result)
}

const arrOfObj: (data: Data) => { [key: string]: string | number }[] = data => {
  return data['value'].map(value => myObject(data.header, value))
}

console.log(arrOfObj(data)[0])

// chunk

interface ArrayConstructor {
  from(arrayLike: any, mapFn?: any): Array<any>
}

const chunk: (arr: number[], size: number) => number[][] = (arr, size) => {
  return Array.from(
    { length: Math.ceil(arr.length / size) },
    (value: any, i: number) => arr.slice(i * size, (i + 1) * size),
  )
}

let arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16]
console.log(chunk(arr, 4))

// compact
const compact: <T>(arr: T[]) => T[] = arr => arr.filter(Boolean)
console.log(compact([0, 1, false, 2, '', 3, 'a', 23, NaN, 's', 34]))

// countBy
const countBy: <T>(
  arr: T[],
  fn: (value: T) => number,
) => { [key: string]: number } = (arr, fn) => {
  let result: { [key: string]: number } = {}
  return arr
    .map(fn)
    .reduce(
      (acc, cur) => ((acc[cur] = (acc[cur] ? acc[cur] : 0) + 1), acc),
      result,
    )
}

console.log(countBy(['one', 'two', 'three'], x => x.length))
console.log(countBy([1, 2, 3, 6, 2, 1, 2, 3, 6], x => x))

// countOccurrences
const countOccurrences: <T>(arr: T[], val: T) => number = (arr, val) => {
  return arr.reduce((acc, cur) => (cur === val ? acc + 1 : acc), 0)
}

console.log(countOccurrences([1, 1, 2, 1, 2, 3], 1))

// difference
const difference: (arrA: number[], arrB: number[]) => number[] = (
  arrA,
  arrB,
) => {
  return arrA.filter(val => !new Set(arrB).has(val))
}

console.log(difference([1, 2, 3], [2, 4, 1]))

// drop
const drop: <T>(arr: T[], n: number) => T[] = (arr, n) => {
  return arr.slice(n)
}

console.log(drop([1, 2, 3, 4, 5], 3))

// dropRight
const dropRight: <T>(arr: T[], n: number) => T[] = (arr, n) => {
  return arr.slice(0, arr.length - n)
}

console.log(dropRight([1, 2, 3], 1))

// dropRightWhile
const dropRightWhile: <T>(arr: T[], fn: (value: T) => boolean) => T[] = (
  arr,
  fn,
) => {
  if (fn(arr.slice(-1)[0]) || arr.length === 0) {
    return arr
  }
  return dropRightWhile(arr.slice(0, -1), fn)
}

console.log(dropRightWhile([5, 6, 7], x1 => x1 < 4))
console.log(dropRightWhile([1, 2, 3, 5, 6, 7], x1 => x1 < 4))

// every Nth
const everyNth: <T>(arr: T[], nth: number) => T[] = (arr, nth) => {
  return arr.filter((ele, ind) => (ind + 1) % nth == 0)
}

console.log(everyNth([1, 2, 3, 4, 5, 6, 7], 4))

// filterFalsy
const filterFalsy: <T>(arr: T[]) => T[] = arr => {
  return arr.filter(Boolean)
}
console.log(filterFalsy(['', true, {}, false, 'sample', 1, 0]))

// filterNonUnique
const filterNonUnique: <T>(arr: T[]) => T[] = arr => {
  return arr.filter(ele => arr.indexOf(ele) == arr.lastIndexOf(ele))
}

console.log(filterNonUnique([1, 2, 3, 4, 5, 4, 5]))

// findLast
const findLast: <T>(arr: T[], fn: (val: T) => boolean) => T | undefined = (
  arr,
  fn,
) => {
  return arr.filter(fn).pop()
}

console.log(findLast([1, 2, 3, 4, 5, 6], x => x % 2 === 0))

// findLastindex
const findLastIndex: <T>(
  arr: T[],
  fn: (val: T | number) => boolean,
) => (number | T)[] | undefined = (arr, fn) => {
  return arr
    .map((ele, ind) => [ind, ele])
    .filter(val => fn(val[1]))
    .pop()
}

console.log(findLastIndex([1, 2, 3, 4], n => n % 2 === 1))

// forEachRight
const forEachRight: <T>(arr: T[], fn: (val: T) => void) => void = (arr, fn) => {
  arr
    .slice(0)
    .reverse()
    .forEach(fn)
}

forEachRight(arr, x => console.log(x * x))

// groupBy
const groupBy: <T>(arr: T[], fn: (val: T) => any) => { [key: string]: T[] } = (
  arr,
  fn,
) => {
  let result: { [key: string]: typeof arr } = {}
  // let result: any = {}
  return arr
    .map(e => [fn(e), e])
    .reduce(
      (acc, cur) => (
        !acc[cur[0]] ? (acc[cur[0]] = [cur[1]]) : acc[cur[0]].push(cur[1]), acc
      ),
      result,
    )
}

console.log(groupBy(['one', 'two', 'three', 'four'], x => x.length))

// head
const head: <T>(arr: T[]) => T = arr => {
  return arr[0]
}

// indexOfAll
const indexOfAll: <T>(arr: T[], val: T) => number[] = (arr, val) => {
  let result: number[] = []
  return arr.reduce(
    (acc, cur, i) => (cur === val ? acc.push(i) : acc, acc),
    result,
  )
}

console.log(indexOfAll([1, 2, 3, 1, 2, 3], 1))

// initial
const initial: <T>(arr: T[]) => T[] = arr => {
  return arr.slice(0, -1)
}

console.log(initial([1, 2, 3]))

//  initialize2DArray
const initialize2DArray: (h: number, w: number, v?: any) => any[][] = (
  h,
  w,
  v = null,
) => {
  return Array.from({ length: h }).map(x => Array.from({ length: w }, () => v))
}

console.log(initialize2DArray(5, 9, 0))
console.log(initialize2DArray(2, 2))

// initializeArrayWithRange
const initializeArrayWithRange: (
  end: number,
  start?: number,
  step?: number,
) => number[] = (end, start = 0, step = 1) => {
  return Array.from(
    { length: (end - start + 1) / step },
    (v: number, i: number) => start + step * i,
  )
}

console.log(initializeArrayWithRange(10))
console.log(initializeArrayWithRange(7, 3))
console.log(initializeArrayWithRange(9, 0, 2))

// initializeArrayWithValues
const initializeArrayWithValues: (n: number, v?: any) => any[] = (n, v = 0) => {
  return Array(n).fill(v)
}

console.log(initializeArrayWithValues(5, 2))

// intersection
const intersection: <T>(arrA: T[], arrB: T[]) => T[] = (arrA, arrB) => {
  return arrA.filter(e => {
    const setB = new Set(arrB)
    return setB.has(e)
  })
}

console.log(intersection([1, 2, 3, 4, 6], [2, 3]))

// intersectionBy
const intersectionBy: <T>(
  arrA: T[],
  arrB: T[],
  fn: (val: T) => T | boolean,
) => T[] = (arrA, arrB, fn) => {
  return arrA.filter(e => {
    const setB = new Set(arrB.map(fn))
    return setB.has(fn(e))
  })
}

console.log(intersectionBy([1, 2, 3, 4, 6], [2], x => x % 2 === 0))
console.log(intersectionBy([2.1, 1.2, 3.6], [2.3, 3.4], Math.floor))

// join

const join: <T>(arr: T[], sep?: string, end?: string) => string = (
  arr,
  sep = ',',
  end = sep,
) => {
  return arr
    .slice(1)
    .reduce(
      (acc, cur, i) =>
        i !== arr.length - 2 ? acc + sep + cur : acc + end + cur,
      `${arr[0]}`,
    )
}

console.log(join([1, 2, 3]))
console.log(join(['pen', 'pineapple', 'apple', 'pen'], ',', '&'))

// last
const last: <T>(arr: T[]) => T = arr => arr[arr.length - 1]

console.log(last([1, 2, 3]))

// maxN
const maxN: <T>(arr: T[], n: number) => T[] = (arr, n) =>
  [...arr].sort().slice(-n)
console.log(maxN([1, 5, 6, 2, 9], 2))

// minN
const minN: <T>(arr: T[], n: number) => T[] = (arr, n) =>
  [...arr].sort().slice(0, n)

console.log(minN([1, 5, 6, 2, 9], 2))

// none
const none: <T>(arr: T[], fn?: (val: T) => boolean) => boolean = (
  arr,
  fn = Boolean,
) => !arr.some(fn)
console.log(none([null, null, null]))
console.log(none([4, null, 'apple']))

// nthElement
const nthElement: <T>(arr: T[], n: number) => T = (arr, n) => arr.slice(n)[0]
console.log(nthElement(['a', 'b', 'c'], 1))

// offset
const offset: <T>(arr: T[], n: number) => T[] = (arr, n) => [
  ...arr.slice(n),
  ...arr.slice(0, n),
]

console.log(offset([1, 2, 3, 4, 5], 2))

// partition
const partition: <T>(arr: T[], filter: (val: T) => boolean) => T[][] = (
  arr,
  filter,
) => {
  let result: typeof arr[] = [[], []]
  return arr.reduce(
    (acc, cur) => (acc[filter(cur) ? 0 : 1].push(cur), acc),
    result,
  )
}

const users = [
  { user: 'barney', age: 36, active: false },
  { user: 'fred', age: 40, active: true },
]
console.log(partition(users, o => o.active))

// sample
const sample: <T>(arr: T[]) => T = arr =>
  arr[Math.floor(Math.random() * arr.length)]

console.log(sample([3, 7, 9, 11]))

// tail
const tail: <T>(arr: T[]) => T[] = arr => (arr.length > 1 ? arr.slice(1) : arr)

console.log(tail([1, 2, 3]))
console.log(tail([1]))

// take
const take: <T>(arr: T[], n: number) => T[] = (arr, n) => arr.slice(0, n)

console.log(take([1, 2, 3], 0))

// takeRight
const takeRight: <T>(arr: T[], n?: number) => T[] = (arr, n = 1) =>
  arr.slice(-n)
console.log(takeRight([1, 2, 3], 2))
console.log(takeRight([1, 2, 3]))

// union
const union: <T>(arrA: T[], arrB: T[]) => T[] = (arrA, arrB) => [
  ...new Set(arrA),
  ...new Set(arrB),
]

console.log(union([1, 2, 8, 11], [6, 6, 7, 2]))

// uniqueElements
const uniqueElements: <T>(arr: T[]) => T[] = arr => [...new Set(arr)]
console.log(uniqueElements([1, 2, 2, 3, 4, 4, 5]))
