import t from 'tcomb'

export default t.refinement(t.String, s => /@/.test(s), 'Email')
