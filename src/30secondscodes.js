const all = (arr, fn = Boolean) => arr.every(fn)
const countBy = (arr, fn) =>
  arr
    .map(fn)
    .reduce((acc, cur) => ((acc[cur] = (acc[cur] ? acc[cur] : 0) + 1), acc), {})

const indexOfAll = (arr, val) =>
  arr.reduce((acc, cur, i) => (cur === val ? acc.push(i) : acc, acc), [])

const join = (arr, sep = ',', end = sep) =>
  arr
    .slice(1)
    .reduce(
      (acc, cur, i) =>
        i !== arr.length - 2 ? acc + sep + cur : acc + end + cur,
      `${arr[0]}`,
    )

const offset = (arr, n) => [...arr.slice(n), ...arr.slice(0, n)]

const initialize2DArray = (h, w, v = null) =>
  Array.from({ length: h }).map(x => Array.from({ length: w }, () => v))

const filterNonUnique = arr =>
  arr.filter(
    (ele, ind) =>
      arr.slice(ind + 1).indexOf(ele) === -1 &&
      arr.slice(0, ind).indexOf(ele) === -1,
  )

const dropRightWhile = (arr, fn) => {
  if (fn(arr.slice(-1)[0]) || arr.length === 0) {
    return arr
  }
  return dropRightWhile(arr.slice(0, -1), fn)
}

const everyNth = (arr, nth) =>
  arr.filter((ele, ind) => (nth > 0 ? (ind + 1) % nth == 0 : false))
const initializeArrayWithRange = (end, start = 0, step = 1) =>
  Array.from({ length: (end - start + 1) / step }, (v, i) => start + step * i)

const findLast = (arr, fn) => arr.filter(fn).pop()

module.exports.all = all
module.exports.countBy = countBy
module.exports.indexOfAll = indexOfAll
module.exports.join = join
module.exports.offset = offset
module.exports.initialize2DArray = initialize2DArray
module.exports.filterNonUnique = filterNonUnique
module.exports.dropRightWhile = dropRightWhile
module.exports.everyNth = everyNth
module.exports.initializeArrayWithRange = initializeArrayWithRange
module.exports.findLast = findLast
