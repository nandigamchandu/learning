import t from 'tcomb'
import Email from './Email'

export default t.struct({
  email: Email,
  name: t.String,
  surname: t.String,
})
