import * as t from 'io-ts'
import { Decoder } from 'io-ts/lib/index'
import { PathReporter } from 'io-ts/lib/PathReporter'

const Name = t.string
const validationResult = Name.decode('lorefnon')
console.log(validationResult.isRight())

// combine these types through combinator to build composite type

const User = t.type({
  name: t.string,
  userId: t.number,
})

const result1 = User.decode({ userId: 1, name: 'Giulio' })
const result2 = User.decode({ userId: '1', name: 'Giulio' })
// validation succeeded
console.log(result1)
// validation failed
console.log(result2)

console.log(PathReporter.report(result1))
console.log(PathReporter.report(result2))

// We can define our own reporter

const getPaths = <A>(v: t.Validation<A>): Array<string> => {
  return v.fold(
    errors =>
      errors.map(error => error.context.map(({ key }) => key).join('.')),
    () => ['no errors'],
  )
}

console.log(getPaths(User.decode({ userId: 1, name: 'Giulio' })))
console.log(getPaths(User.decode({ userId: '1', name: 'Giulio' })))
console.log(getPaths(User.decode({ userId: '1', name: 1 })))

// Static types can be extracted from codes using TypeOf
type User = t.TypeOf<typeof User>
