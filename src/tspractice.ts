import * as mod from './myModule'
import MyClass, { bar, foo } from './myModule'
import AnotherClass from './myModule'
import { permuteDomain } from 'tough-cookie'

foo()
console.log(bar)
let myInstance = new MyClass()
console.log(myInstance)
console.log('foo.bat: ' + mod.bar)

// Enhancements made to functions in TypeScript

// Optional parameter
function getRange(max, min, exclusive?) {
  return exclusive
}

console.log(getRange(5, 6))

// Making a parameter optional with a different value as the default

function getRange1(max, min = 0, exclusive = true) {
  return exclusive
}

console.log(getRange1(5))

// ...rest parameter
function publish(topic, ...args) {
  return args
}

console.log(publish('web', 'javascript', 'typescript', 'node'))

// arrow function provides a new shorthand syntax, and also changes the way the this keyword works.
let obj = {
  a: 1,
  arrow: () => {
    console.log('arrow', this)
  },
  regular: function() {
    console.log('regular', this)
  },
}
obj.arrow()
obj.regular()

// object shorthand notations
const myvar = 5
let a = {
  myvar,
  bar() {
    return 'i am bar'
  },
}
console.log(a.myvar)
console.log(a.bar())

// Destructuring
let [x, y] = [10, 20]
console.log(x, y)

// Destructuring objects
let {
  place,
  location: [x1, y1],
} = { place: 'test', location: [1000, 2000] }

console.log(place, x, y)

// Types
function toNumber(numberString: string): number {
  const num: number = parseFloat(numberString)
  return num
}

console.log(toNumber('4546'))

function numberStringSwap(value: any, radix: number = 10): any {
  if (typeof value === 'string') {
    return parseInt(value, radix)
  } else if (typeof value === 'number') {
    return String(value)
  }
}

const num1 = numberStringSwap('12345') as number
const str1 = numberStringSwap(12345) as string
console.log(typeof num1)
console.log(typeof str1)

// Object types
let point: { x: number; y: number }
// point = { x: 'zero', y: 0 } // Type 'string' is not assignable to type 'number'.
// point = { x: 5, y: 9, z: 10 } // 'z' does not exist in type
point = { x: 5, y: 6 }
console.log('point', point)

const otherPoint = { x: 11, y: 11, z: 11 }
point = otherPoint
console.log('otherpoint', point)

let point2: typeof point
point2 = { x: 5, y: 6 }
console.log('point2', point2)

interface Point {
  x: number
  y: number
}
const point3: Point = { x: 10, y: 0 }

interface Point3d extends Point {
  z: number
}

interface Point3dOptional {
  x: number
  y: number
  z?: number
}

let point2d: Point3dOptional
point2d = { x: 1, y: 2 }
console.log('point2d', point2d)

let point3d: Point3dOptional
point3d = { x: 1, y: 2, z: 3 }
console.log('point3d', point3d)

// add method to an object
interface PointWithMethod {
  x: number
  y: number
  toGeo(): Point
}
let point4: PointWithMethod
// point4 = { x: 1, y: 2 } // Property 'toGeo' is missing in type

point4 = {
  x: 1,
  y: 2,
  toGeo: () => {
    return { x: point4.x, y: point4.y }
  },
}
console.log('point4', point4)

// methods can also be optional
interface PointMethodOption {
  x: number
  y: number
  toGeo?(): Point
}
let point5: PointMethodOption
point5 = {
  x: 1,
  y: 2,
}
console.log('point5', point5)

// Object that are intended to be used as hash  maps or ordered lists
interface HashMapOfPoints {
  x: number
  y: number
  [key: string]: number
}
console.log('point.x', point.x)
// point.w = 10 // Property 'w' does not exist on type
point['w1'] = 10
point.x = 611
console.log('point.x', point.x)

let point6: HashMapOfPoints
point6 = { x: 45, y: 48 }
console.log('point6', point6)
point6.z = 10
console.log('point6', point6)

const Foo = 'Foo'
const Bar = 11

interface MyInterface {
  [Foo]: number
  [Bar]: string
}

let myinterface: MyInterface = { Foo: 10, 11: 'bar' }
console.log('myinterface', myinterface)
console.log('myinterface[Foo]', myinterface[Foo])
myinterface[Foo] = 15
console.log('myinterface[Foo]', myinterface[Foo])

// Tuple types
function draw(...point: [number, number, number?]): void {
  const [x, y, z] = point
  console.log('draw', ...point, 'x=', x, 'y=', y, 'z=', z)
}

// draw(100) // Expected 2-3 arguments, but got 1
draw(100, 200)
draw(100, 200, 300)

// Function types
let printPoint: (point: Point) => string

// Overloaded functions
function numberStringSwap1(value: number, radix?: number): string
function numberStringSwap1(value: string): number
function numberStringSwap1(value: any, radix: number = 10): any {
  if (typeof value === 'string') {
    return parseInt(value, radix)
  } else if (typeof value === 'number') {
    return String(value)
  }
}
// Strict function type

class Animal {
  breath() {}
}
class Dog extends Animal {
  bark() {}
}

class Cat extends Animal {
  meow() {}
}

let f1: (x: Animal) => void = x => x.breath()
let f2: (x: Dog) => void = x => x.bark()
let f3: (x: Cat) => void = x => x.meow()

// f2 = f3 // Property 'meow' is missing in type 'Dog' but required in type 'Cat'.
const c = new Cat()
// f1(c) // run time error: x.bark is not a function

// Generic type

interface Arrayish<T> {
  map<U>(
    callback: (value: T, index: number, array: Arrayish<T>) => U,
    thisArg?: any,
  ): Array<U>
}

const arrayOfStrings: Arrayish<string> = ['a', 'b', 'c']
console.log(arrayOfStrings)
//Types of property 'map' are incompatible

const arrayOfCharCodes: Arrayish<number> = arrayOfStrings.map(x =>
  x.charCodeAt(0),
)
console.log(arrayOfCharCodes)
// declare a default type for generic

function createArrayish<T = string>(...args: T[]): Arrayish<T> {
  return args
}

console.log(createArrayish('fdsf', 'fwef', 'fsdf'))
console.log(createArrayish())

// Union type
// the union type is used to indicate that a parameter or variable can contain more than one type
function byId(element: string | number): string {
  if (typeof element === 'string') {
    return 'i am string'
  } else {
    return `${element}`
  }
}

// byId(true)  // Argument of type 'true' is not assignable to parameter of type 'string | number'
console.log(byId(5))
console.log(byId('five'))

// Intersection types

interface Foo {
  name: string
  count: number
  id: number
}

interface Bar {
  name: string
  age: number
  id: number
}

let foobar: Foo & Bar
// foobar = { name: 'Chandu', age: 27 } //  is missing the following properties from type 'Foo': count, id
// foobar = { name: 'Chandu', id: 'dsfd' } // 'string' is not assignable to type 'string & number'.
foobar = { name: 'Chandu', id: 5, count: 5, age: 27 }

// Conditional types
// T extends U? X: Y
declare function addOrConcat<T extends number | string>(
  x: T,
): T extends number ? number : string

// Using the typeof type guard
function lower(x: string | string[]) {
  if (typeof x === 'string') {
    return x.toLowerCase()
  } else {
    return x.reduce((val: string, next: string) => {
      return (val += `, ${next.toLowerCase()}`)
    }, '')
  }
}

console.log(lower('HELLO'))
// console.log(lower(true)) // Argument of type 'true' is not assignable to parameter of type 'string | string[]'
console.log(lower(['NAMDIGAM', 'CHANDU', 'PRASAD']))

// Using the in type guard

function plot(point: Point): void {
  if ('z' in point) {
    console.log('point3d')
  } else {
    console.log('point2d')
  }
}

plot(point2d)
plot(point3d)
